// Fill out your copyright notice in the Description page of Project Settings.


#include "Barrier.h"
#include "SnakeBase.h"

void ABarrier::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (bIsHead)
	{
		if (IsValid(Snake))
		{
			Snake->Destroy();
		}
	}
}