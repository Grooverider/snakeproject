// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakePlayground.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Engine/Classes/Components/SphereComponent.h"
#include "SnakeBase.h"
#include "Food.h"

// Sets default values
ASnakePlayground::ASnakePlayground()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RootComponent"));
	RootComponent = MeshComponent;
	TopRightSphere = CreateDefaultSubobject<USphereComponent>(TEXT("TopRightSphere"));
	TopRightSphere->AttachToComponent(MeshComponent, FAttachmentTransformRules(EAttachmentRule::KeepWorld, true));
	BottomLeftSphere = CreateDefaultSubobject<USphereComponent>(TEXT("BottomLeftSphere"));
	BottomLeftSphere->AttachToComponent(MeshComponent, FAttachmentTransformRules(EAttachmentRule::KeepWorld, true));

}

// Called when the game starts or when spawned
void ASnakePlayground::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ASnakePlayground::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakePlayground::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		
		if (IsValid(Snake))
		{
			Snake->Destroy();
		}
	}
}
