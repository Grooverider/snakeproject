// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusFood.h"
#include "SnakeBase.h"
#include "Engine/Classes/Kismet/KismetMathLibrary.h"

ABonusFood::ABonusFood()
{
	NewSpeed = 0.3;
	BonusTime = 5;
}

void ABonusFood::Interact(AActor* Interactor, bool bIsHead)
{
	bool bIsBarrier(UKismetMathLibrary::RandomBoolWithWeight(BarrierChance));
	bool bIsBonus(UKismetMathLibrary::RandomBoolWithWeight(BonusChance));
	auto Snake = Cast<ASnakeBase>(Interactor);
	float PreviousSpeed = Snake->GetSnakeSpeed();
	if (bIsHead)
	{
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Snake->SetSnakeSpeed(NewSpeed);
			SetBonusTimer(Interactor, PreviousSpeed);
			if (bIsBarrier)
			{
				AddBarrier();
				AddFood();
			}
			else if (bIsBonus)
			{
				AddBonus();
			}
			else
			{
				AddFood();
			}
			Snake->FoodScore++;
			this->Destroy();
		}
	}
}

void ABonusFood::SetBonusTimer(AActor* Interactor, float Speed)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	FTimerDelegate Delegate;
	Delegate.BindUFunction(Snake, FName("SetSnakeSpeed"), Speed);
	GetWorldTimerManager().SetTimer(TimerHandle,
		Delegate, BonusTime, false);
}