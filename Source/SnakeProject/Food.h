// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Food.generated.h"

UCLASS()
class SNAKEPROJECT_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> FoodClass;
	
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> BarrierClass;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AFood> BonusClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;
	
	UPROPERTY(EditDefaultsOnly)
	float BarrierChance;

	UPROPERTY(EditDefaultsOnly)
	float BonusChance;	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	UFUNCTION(BlueprintCallable)
	void AddFood(int FoodAmount = 1);

	FVector GetEmptyPoint(float Radius, const TArray<AActor*> IgnoredActors,
		bool DrawDebugContext, int TryCount, bool& bCanAdd);

	//void HandleBonus(AActor* Interactor);

	void AddBarrier();

	void AddBonus();
};