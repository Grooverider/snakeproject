// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "Engine/Classes/Kismet/KismetMathLibrary.h"
#include "GameInstanceBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;

	BarrierChance = 0.7;
	BonusChance = 0.3;

}


// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	bool bIsBarrier(UKismetMathLibrary::RandomBoolWithWeight(BarrierChance));
	bool bIsBonus(UKismetMathLibrary::RandomBoolWithWeight(BonusChance));
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (bIsHead)
	{	
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			if (bIsBarrier)
			{
				AddBarrier();
				AddFood();
			}
			else if (bIsBonus)
			{
				AddBonus();
			}
			else
			{
				AddFood();
			}
			Snake->FoodScore++;
			this->Destroy();
		}
	}
}

FVector AFood::GetEmptyPoint(float Radius, const TArray<AActor*> IgnoredActors,
	bool DrawDebugContext, int TryCount, bool& bCanAdd)
{
	bCanAdd = false;
	UWorld* World = GetWorld();
	UGameInstanceBase* GameInstance = Cast<UGameInstanceBase>(GetGameInstance());
	if (World == nullptr || GameInstance == nullptr)
	{
		return FVector::ZeroVector;
	}

	EDrawDebugTrace::Type DebugTrace = EDrawDebugTrace::None;
	if (DrawDebugContext)
	{
		DebugTrace = EDrawDebugTrace::ForDuration;
	}

	int Counter = 0;
	FHitResult HitResult;

	while (Counter < TryCount)
	{
		FVector EmptyPoint(
			FMath::RandRange(GameInstance->BottomLeftCorner.X, GameInstance->TopRightCorner.X),
			FMath::RandRange(GameInstance->BottomLeftCorner.Y, GameInstance->TopRightCorner.Y), 0
		);

		UKismetSystemLibrary::SphereTraceSingle(World, EmptyPoint, EmptyPoint, Radius,
			UEngineTypes::ConvertToTraceType(ECC_Visibility), true,
			IgnoredActors, DebugTrace, HitResult, true,
			FLinearColor::Green, FLinearColor::Red, 5.0F);

		if (HitResult.bBlockingHit == false)
		{
			bCanAdd = true;
			return EmptyPoint;
		}
		Counter++;
	}
	return FVector::ZeroVector;
}

void AFood::AddFood(int FoodAmount)
{
	bool bCanAdd;
	TArray<AActor*> IgnoredActors;
	for (int i = 0; i < FoodAmount; i++)
	{
		FVector NewLocation = GetEmptyPoint(30, IgnoredActors, true, 150, bCanAdd);
		if (!bCanAdd)
		{
			return;
		}
		FTransform FoodTransform(NewLocation);
		GetWorld()->SpawnActor<AFood>(FoodClass, FoodTransform);
	}
}

void AFood::AddBarrier()
{
	bool bCanAdd;
	TArray<AActor*> IgnoredActors;
	FVector NewLocation = GetEmptyPoint(30, IgnoredActors, true, 150, bCanAdd);
	if (!bCanAdd)
	{
		return;
	}
	FTransform BarrierTransform(NewLocation);
	GetWorld()->SpawnActor<AFood>(BarrierClass, BarrierTransform);
}

void AFood::AddBonus()
{
	bool bCanAdd;
	TArray<AActor*> IgnoredActors;
	FVector NewLocation = GetEmptyPoint(30, IgnoredActors, true, 150, bCanAdd);
	if (!bCanAdd)
	{
		return;
	}
	FTransform BonusTransform(NewLocation);
	GetWorld()->SpawnActor<AFood>(BonusClass, BonusTransform);
}
