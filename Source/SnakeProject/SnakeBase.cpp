// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 60.f;
	
	MovementSpeed = 0.4;
	
	LastMoveDirection = EMovementDirection::RIGHT;

	StartLocation = FVector(0, -150, 0);
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	
	AddSnakeElement(4);

	SetActorTickInterval(MovementSpeed);

}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation, CurrentLocation;
		if (SnakeElements.Num() == 0)
		{
			NewLocation = StartLocation;
		}
		else if (SnakeElements.Num() == 1)
		{
			CurrentLocation = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
			if (LastMoveDirection == EMovementDirection::UP)
			{
				NewLocation = FVector(CurrentLocation.X - ElementSize, CurrentLocation.Y, 0);
			}
			else if (LastMoveDirection == EMovementDirection::DOWN)
			{
				NewLocation = FVector(CurrentLocation.X + ElementSize, CurrentLocation.Y, 0);
			}
			else if (LastMoveDirection == EMovementDirection::RIGHT || 
				LastMoveDirection == EMovementDirection::STANDBY)
			{
				NewLocation = FVector(CurrentLocation.X, CurrentLocation.Y - ElementSize, 0);
			}
			else if (LastMoveDirection == EMovementDirection::LEFT)
			{
				NewLocation = FVector(CurrentLocation.X, CurrentLocation.Y + ElementSize, 0);
			}
		}
		else
		{
			CurrentLocation = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
			FVector PreviousLocation = SnakeElements[SnakeElements.Num() - 2]->GetActorLocation();
			if (CurrentLocation.X > PreviousLocation.X)
			{
				NewLocation = FVector(CurrentLocation.X + ElementSize, CurrentLocation.Y, 0);
			}
			else if (CurrentLocation.X < PreviousLocation.X)
			{
				NewLocation = FVector(CurrentLocation.X - ElementSize, CurrentLocation.Y, 0);
			}
			else if (CurrentLocation.Y > PreviousLocation.Y)
			{
				NewLocation = FVector(CurrentLocation.X, CurrentLocation.Y + ElementSize, 0);
			}
			else
			{
				NewLocation = FVector(CurrentLocation.X, CurrentLocation.Y - ElementSize, 0);
			}
		
		}
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		int32 ElementIndex = SnakeElements.Add(NewSnakeElement);
		
		if (ElementIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= ElementSize;
		break;
	case EMovementDirection::STANDBY:
		MovementVector = FVector::ZeroVector;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PreviousElement = SnakeElements[i - 1];
		FVector PreviousLocation = PreviousElement->GetActorLocation();
		CurrentElement->SetActorLocation(PreviousLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	
	bIsMoveAllowed = true;
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

float ASnakeBase::GetSnakeSpeed()
{
	return MovementSpeed;
}

void ASnakeBase::SetSnakeSpeed(float Speed)
{
	SetActorTickInterval(Speed);
}
