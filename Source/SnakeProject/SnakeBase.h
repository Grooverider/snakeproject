// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	RIGHT,
	LEFT,
	STANDBY
};

UCLASS()
class SNAKEPROJECT_API ASnakeBase : public AActor
{
	GENERATED_BODY()
		
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY(EditAnywhere)
	float ElementSize;

	UPROPERTY()
	EMovementDirection LastMoveDirection;

	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY(BlueprintReadOnly)
	bool bIsMoveAllowed;

	UPROPERTY(EditDefaultsOnly)
	FVector StartLocation;

	UPROPERTY(BlueprintReadOnly)
	int32 FoodScore;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category="SnakePawn")
	void AddSnakeElement(int ElementsNum = 1);
	
	UFUNCTION(BlueprintCallable)
	void Move();

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	UFUNCTION()
	float GetSnakeSpeed();
	
	UFUNCTION()
	void SetSnakeSpeed(float Speed);
};
