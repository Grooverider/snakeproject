// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "BonusFood.generated.h"

UCLASS()
class SNAKEPROJECT_API ABonusFood : public AFood
{
	GENERATED_BODY()

public:
	ABonusFood();

	UPROPERTY(EditDefaultsOnly)
	float NewSpeed;

	UPROPERTY(EditDefaultsOnly)
	float BonusTime;
	
	FTimerHandle TimerHandle;

public:

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	void SetBonusTimer(AActor* Interactor,float PreviousSpeed);
};