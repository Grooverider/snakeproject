// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"
#include "Food.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	
	SetActorRotation(FRotator(-90, 0, 0));

	CreateSnakeActor();

	StartPosition = FVector(0, 350, 0);

}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if(SnakeActor->bIsMoveAllowed)
	{ 
		if (IsValid(SnakeActor))
		{
			if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN)
			{
				SnakeActor->LastMoveDirection = EMovementDirection::UP;
				SnakeActor->bIsMoveAllowed = false;
			}
			else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP)
			{
				SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
				SnakeActor->bIsMoveAllowed = false;
			}
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (SnakeActor->bIsMoveAllowed)
	{
		if (IsValid(SnakeActor))
		{
			if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT)
			{
				SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
				SnakeActor->bIsMoveAllowed = false;
			}
			else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT)
			{
				SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
				SnakeActor->bIsMoveAllowed = false;
			}
		}
	}
}

int32 APlayerPawnBase::GetScore()
{
	if (SnakeActor)
	{
		return SnakeActor->FoodScore;
	}
	return 0;
}

//int32 APlayerPawnBase::GetGameStage()
//{
//	return GameStage;
//}
//
//void APlayerPawnBase::SetGameStage(int32 Stage)
//{
//	GameStage = Stage;
//}
